# -------------------------
# pseudo_exponential_algo_test.py
# -------------------------

# -------------------------
# Description:
# - Implementation of pseudo-exponential algorithm for fractional models
#
# Last edit: 25.04. 2022
# -------------------------

import numpy as np
import matplotlib.pyplot as plt
import math as m
import js_workdir.utils.mittag_leffler as ml


def force(t_i, type):
    if type == "sin":
        return m.sin(t_i)
    elif type == "heav":
        return 1
    else:
        return t_i


# Parameters
times = np.linspace(0, 10.0, 1000)
dt = times[1] - times[0]
f_type = "sin"
tau = 5
alpha = 0.000001
E = 1.0
#g = m.gamma(1 - alpha)
g2 = m.gamma(1 + alpha)
g3 = m.gamma(2 - alpha)

epsilon = [0]
epsilon_prev = 0
fce = [0]
sigma_old = 0

ml_eval = ml.ml(-(dt/tau)**(alpha), alpha)
print(ml_eval)
ml_eval_2 = ml.ml(-(dt/tau)**alpha, alpha, beta=2)

exact = [1/(E*g2)*(ts/tau)**alpha for ts in times]

for ts in times[1:]:
    fi = force(ts, f_type)
    #nom = fi - sigma_old*ml_eval + epsilon_prev*E*(tau)**alpha/dt*(1 - ml_eval)
    #denom = E*(tau)**alpha/dt*(1 - ml_eval)
    #nom = (fi - sigma_old)*ml_eval + epsilon_prev*E*ml_eval_2
    #denom = E*ml_eval_2
    # attemp 3
    #nom = fi - sigma_old*ml_eval + epsilon_prev*E*ml_eval_2
    #denom = E*ml_eval_2
    #temp = nom/denom
    temp = epsilon_prev - sigma_old/E + fi/ml_eval_2
    sigma_old = fi
    #sigma_old = sigma_old*ml_eval + E*(temp - epsilon_prev)*ml_eval_2
    fce.append(3*fi)
    epsilon.append(temp)
    epsilon_prev = temp

#print(exact[-1] - epsilon[-1])

#plt.plot(times, exact)
plt.plot(times, epsilon, "x")
#plt.plot(times, fce)
plt.show()
