# -------------------------
# pseudo_exponential_algo_test_sigma.py
# -------------------------

# -------------------------
# Description:
# - Implementation of pseudo-exponential algorithm for fractional models
# - Derived using sigma discretization
#
# Last edit: 02.05. 2022
# -------------------------

import numpy as np
import matplotlib.pyplot as plt
import math as m
import js_workdir.utils.mittag_leffler as ml


def force(t_i, type):
    if type == "sin":
        return m.sin(t_i)
    elif type == "heav":
        return 1
    else:
        return t_i


# Parameters
times = np.linspace(0, 10.0, 1000)
dt = times[1] - times[0]
f_type = "sin"
tau = 10
alpha = 0.00001
E = 2.0
eta = tau*E

#g = m.gamma(1 - alpha)
g2 = m.gamma(1 + alpha)
g3 = m.gamma(2 - alpha)

epsilon = [0]
epsilon2 = [0]
epsilon_prev = 0
fce = [0]
sigma_old = 0
sigma_old_2 = 0
epsilon_prev_2 = 0

dt2=1
dt2 = dt**alpha
#dt2 = dt
#ksi = 0.5*(alpha + 1)
ksi = 1

ml_eval = ml.ml(-(dt/tau)**(alpha), alpha)
print(-(dt/tau)**(alpha))
print(ml_eval-1, m.expm1(-dt/tau))
print(ml_eval, m.exp(-dt / tau))
#ml_eval_2 = ml.ml(-(dt/tau)**alpha, alpha, beta=2)

exact = [1/(E*g2)*(ts/tau)**alpha for ts in times]

for ts in times[1:]:
    fi = force(ts, f_type)
    #temp = epsilon_prev + (fi + sigma_old)/(2*E*g2)*(dt/tau)**alpha
    #nom = eta/dt*(1 - m.exp(-dt/tau))
    denom2 = -eta/dt*m.expm1(-dt/tau)
    denom = tau**alpha*E/dt2*(1-ml_eval)
    #denom = fi - sigma_old_2*m.exp(-dt/tau) + epsilon_prev_2*eta/dt*(1 - m.exp(-dt/tau))
    nom2 = fi - sigma_old_2 * m.exp(-dt / tau) - epsilon_prev_2 * eta / dt * m.expm1(-dt/tau)
    nom = ksi*fi - sigma_old*ml_eval + epsilon_prev*tau**alpha*E/dt2*(1-ml_eval)
    temp2 = nom2/denom2
    temp = nom/denom
    sigma_old = fi
    sigma_old_2 = fi
    #sigma_old = sigma_old*ml_eval + E*(temp - epsilon_prev)*ml_eval_2
    fce.append(fi/E)
    epsilon.append(temp)
    epsilon2.append(temp2)
    epsilon_prev = temp
    epsilon_prev_2 = temp2

#print(exact[-1] - epsilon[-1])

#plt.plot(times, exact)
plt.plot(times, epsilon2, "x", label="exp")
plt.plot(times, epsilon, "x", label="fr-exp")
plt.plot(times, fce)
plt.legend()
plt.show()
