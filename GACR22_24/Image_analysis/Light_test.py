import cv2
import numpy as np


def plot_image(img_i):
    imS = cv2.resize(img_i, (1600, 1000))
    cv2.imshow("Image", imS)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def edges_test(imgg, oname):
    outputpath = "Output/" + oname
    plot_image(imgg)

    cv2.imwrite(outputpath + "_01_origin.png", imgg)

    img_gray = cv2.cvtColor(imgg, cv2.COLOR_BGR2GRAY)
    img_blur = cv2.fastNlMeansDenoising(img_gray, None, 6, 7, 21)
    plot_image(img_blur)

    #par = 1
    #img_blur = cv2.GaussianBlur(img_gray, (par, par), cv2.BORDER_DEFAULT)
    #plot_image(img_blur)


    # Sobel Edge Detection
    sobelx = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=1, dy=0, ksize=5)  # Sobel Edge Detection on the X axis
    sobely = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=0, dy=1, ksize=5)  # Sobel Edge Detection on the Y axis
    sobelxy = cv2.Sobel(src=img_blur, ddepth=cv2.CV_64F, dx=1, dy=1, ksize=5)  # Combined X and Y Sobel Edge Detection

    # Display Sobel Edge Detection Images
    plot_image(sobelx)
    plot_image(sobely)
    plot_image(sobelxy)

    cv2.imwrite(outputpath + "_02_sobelx.png", sobelx)
    cv2.imwrite(outputpath + "_03_sobely.png", sobely)
    cv2.imwrite(outputpath + "_04_sobelxy.png", sobelxy)

    # Canny Edge Detection
    edges = cv2.Canny(image=img_gray, threshold1=100, threshold2=200)  # Canny Edge Detection
    plot_image(edges)
    cv2.imwrite(outputpath + "_05_canny.png", edges)

    # Laplacian
    lapla = cv2.Laplacian(img_blur, cv2.CV_64F)
    plot_image(lapla)
    cv2.imwrite(outputpath + "_06_laplacian.png", lapla)

    # Second Sobel
    sobelx64f = cv2.Sobel(img_blur, cv2.CV_64F, 1, 0, ksize=5)
    abs_sobel64f = np.absolute(sobelx64f)
    sobel_8u = np.uint8(abs_sobel64f)

    plot_image(sobel_8u)
    cv2.imwrite(outputpath + "_07_sobelextra.png", sobel_8u)


img01 = cv2.imread("Data/DSC00025.JPG")
img02 = cv2.imread("Data/DSC00027.JPG")

edges_test(img01, "img01")
edges_test(img02, "img02")
