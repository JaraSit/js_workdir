# -------------------------
# temperature_foil_test_11L.py
# -------------------------

# -------------------------
# Description:
# - Influece of temperature on beam response
#
# Last edit: 25.04. 2022
# -------------------------

import fenics as fe
import numpy as np
import matplotlib.pyplot as plt
import math
import os

#from samba.netcmd.main import cache_loader
#import utils.functions as fu
import js_workdir.utils.functions as fu
import scipy.stats as ss


# Mesh creator
def prepare_mesh():
    # mesh = fe.Mesh("Meshes/QS_4PB_1D_ref.xml")
    mesh = fe.IntervalMesh(550, 0.0, 0.55)
    return mesh


def append_file(folder, file_name, files):
    file_temp = fe.XDMFFile(folder + "/" + file_name)
    # file_temp.parameters["flush_output"] = True
    files.append(file_temp)


def prepare_files(folder, params, n_glass, n_layers, V):
    # Post-process files
    # File for displacement field
    files_u = []
    append_file(folder, "displ_w.xdmf", files_u)
    for i in range(n_glass):
        append_file(folder, "displ_w_" + str(i) + ".xdmf", files_u)
        append_file(folder, "displ_phi_" + str(i) + ".xdmf", files_u)

    # File for damage field
    files_d = []
    for i in range(n_glass):
        append_file(folder, "damage_" + str(i) + ".xdmf", files_d)
        files_d[i].write_checkpoint(fe.interpolate(fe.Constant(0.0), V), "f", 0.0,  fe.XDMFFile.Encoding.HDF5, False)

    # Create log_file
    if not os.path.exists(folder):
        os.makedirs(folder)
    log_file = open(folder + "/log_file.txt", "w+")
    log_file.write("Log_file.txt\n")
    log_file.write(
        "eps_staggered = " + str(params.tol_sl) + ", max_iters_staggered = " + str(params.max_iter) +
        ", criterium = " + str(params.conv_crit) + ", True = converges\n")
    log_file.write("time\titer_numbers\tconverges\n")
    log_file.close()

    # Create stress_file
    stress_file = open(folder + "/stress_data.txt", "w+")
    stress_file.write("Stress_data.txt\n")
    stress_file.write(
        "S = Stress, E = Strain, T=Top surface in middle of beam, B=Bottom surface in middle of beam, TQ=Top surface in quarter ob beam\n")
    stress_file.write("time\tu_B\tE_x_B\tS_x_B\tE_x_T\tS_x_T\tE_x_QT\tS_x_QT\n")
    stress_file.close()

    # Create reacts_file
    react_file = open(folder + "/reaction_data.txt", "w+")
    react_file.write("Reaction_data.txt\n")
    react_file.write("time\tu_B\treaction\n")
    react_file.close()

    return files_u, files_d


def glass_9l_lg_solver(time_space, fts, folder, params, vis_mat, load_velocity):

    # Definition of supporting point
    def left_point(x):
        return fe.near(x[0], l_off)

    # Definition of loading point
    def left_load_point(x):
        return fe.near(x[0], l_x / 2 - l_0 / 2)

    # Definition of symmetry axis location
    def symmetry_axis(x, on_boundary):
        return fe.near(x[0], l_x/2) and on_boundary

    # Staggered loop
    def solve_staggered():
        # Time loop
        en_prev = 0.0
        for j in range(0, len(time_space)):
            t = time_space[j]

            u_d.t = t

            # Update of foil stiffness
            update_foil(t)

            print(Es[1])
            print(Gs[1].g)

            # Informative print of time instant
            print("Time instant: ", t)

            ite = 1
            err = 50.0

            converges = "False"

            # Staggered loop
            while err > params.tol_sl:

                # Damage and displacement solutions
                solve_displacement()

                solve_damage()

                if params.conv_crit == "rel_inc":
                    # Damage and displacement increments
                    du.assign(u - u_old)
                    dd.assign(d - d_old)

                    du_split = du.split(deepcopy=True)
                    u_split = u.split(deepcopy=True)
                    err_u = fe.norm(du_split[0]) / fe.norm(u_split[0])

                    # Errors - damage is not standardized
                    #err_u = fe.norm(du) / fe.norm(u)
                    err_d = fe.norm(dd)
                    err = max(err_u, err_d)

                    print("iter", ite, "errors", err_u, err_d)

                    u_old.assign(u)
                    d_old.assign(d)
                elif params.conv_crit == "energy":
                    # en = get_total_energy(u, d)
                    # err = abs((en - en_prev) / en)
                    # print("iter", ite, ", energy_error: ", err)
                    # en_prev = en
                    raise Exception("Energy criterium not implement yet!")
                else:
                    raise Exception("conv_crit must be energy/rel_inc!")

                ite += 1

                # Max iterations condition
                if ite > params.max_iter:
                    print("max iterations reached")
                    break

                if err <= params.tol_sl:
                    converges = "True"

            # tkns = np.linspace(-0.5*h[0], h_tot-0.51*h[0], 100)
            # tkns_str = [get_stress_in_point(fe.Point((0.5, 0.0, pi)), u, d)[1] for pi in tkns]
            # plt.plot(tkns, tkns_str)
            # plt.show()

            d_min.assign(d)

            save_displacement_and_damage(file_u, file_d, t, n_lay, glass_num)

            save_stress_reaction_sd(t)

            d_split = d.split(deepcopy=True)
            # if np.amax(d_split[0].vector()[:]) > 0.9 or np.amax(d_split[1].vector()[:]) > 0.9 or np.amax(d_split[2].vector()[:]) > 0.9:
            #     plot_damage(d_split)
            if np.amax(d_split[0].vector()[:]) > 0.99 and np.amax(d_split[1].vector()[:]) > 0.99 and np.amax(d_split[2].vector()[:]) > 0.99 and np.amax(d_split[3].vector()[:]) > 0.99:
                break

            file_object = open(folder + "/log_file.txt", "a")
            file_object.write(str(round(t, 5)) + "\t" + str(ite) + "\t" + str(converges) + "\n")
            file_object.close()

        [file_i.close() for file_i in file_u]
        [file_i.close() for file_i in file_d]

    def plot_damage(d_sp):
        fig, axs = plt.subplots(glass_num, 1)
        x = mesh.coordinates()
        plt.subplot(311)
        fe.plot(d_sp[0])
        plt.subplot(312)
        fe.plot(d_sp[1])
        plt.subplot(313)
        fe.plot(d_sp[2])
        # for o in range(glass_num):
        #     plot_object = fe.plot(d_sp[o])
        #     print(plot_object)
        #     axs[o].plot(plot_object)
        #     axs[o].plot(x, d_sp[o].vector()[:])
        # plt.tight_layout()
        plt.xlabel("x coord. [m]")
        plt.ylabel("damage")
        #     plt.figure()
        #     fe.plot(d_sp[0])
        plt.show()

    # Displacement solver
    def solve_displacement():
        u.vector()[:] = np.random.rand(u.vector().size())
        H = fe.derivative(u_form, u, fe.TrialFunction(V))
        problem = fe.NonlinearVariationalProblem(u_form, u, BC_u, H)
        solver = fe.NonlinearVariationalSolver(problem)
        parameters = {"newton_solver": {"error_on_nonconvergence": False,
                                        "relative_tolerance": params.tol_nm}}
        solver.parameters.update(parameters)
        solver.solve()

    # Damage snes variational inequalities solver based on Newton's method
    def solve_damage():
        lower = d_min
        upper = fe.interpolate(fe.Constant(glass_num*[1.0]), W)
        #upper = fe.interpolate(fe.Constant(1.0), W)

        # Solution of damage formulationw
        H = fe.derivative(d_form, d, fe.TrialFunction(W))

        snes_solver_parameters = {"nonlinear_solver": "snes",
                                  "snes_solver": {"linear_solver": "lu",
                                                  "relative_tolerance": 1.0e-6,
                                                  "absolute_tolerance": 1.0e-6,
                                                  "maximum_iterations": 50,
                                                  "report": True,
                                                  "error_on_nonconvergence": False,
                                                  "line_search": "basic"}}

        problem = fe.NonlinearVariationalProblem(d_form, d, BC_d, H)
        problem.set_bounds(lower, upper)

        solver = fe.NonlinearVariationalSolver(problem)
        solver.parameters.update(snes_solver_parameters)
        solver.solve()

    # Save displacements and damages into files
    def save_displacement_and_damage(file_u_i, file_d_i, t_i, n_layers, n_glass):
        u_ = u.split(deepcopy=True)
        file_u_i[0].write(u_[0], t_i)
        for i in range(n_glass):
            file_u_i[2*i + 1].write(u_[2*i + 1], t_i)
            file_u_i[2*i + 2].write(u_[2*i + 2], t_i)

        d_ = d.split(deepcopy=True)
        for i in range(n_glass):
            #file_d_i[i].write(d_[i], t_i)
            file_d_i[i].write_checkpoint(d_[i], "f", t_i, fe.XDMFFile.Encoding.HDF5, True)

    # Save stress and reaction to file
    def save_stress_reaction_sd(t_i):
        stress_file = open(folder + "/stress_data.txt", "a")
        lg_displ = u.split(deepcopy=True)
        ind_bottom = 1
        ind_top = 7
        u_bottom = lg_displ[ind_bottom]
        u_top = lg_displ[ind_top]
        phi_bottom = lg_displ[ind_bottom + 1]
        phi_top = lg_displ[ind_top + 1]
        eps_bottom = u_bottom.dx(0) + phi_bottom.dx(0)*(-0.5*h[0])
        eps_top = u_top.dx(0) + phi_top.dx(0)*(0.5*h[len(h) - 1])
        strain_bottom = fu.local_project(eps_bottom, V0)
        strain_top = fu.local_project(eps_top, V0)
        stress_bottom = fu.local_project((1.0 - d[0])**2*Es[0]*fu.mc_bracket(eps_bottom) - Es[0]*fu.mc_bracket(-eps_bottom), V0)
        stress_top = fu.local_project((1.0 - d[3])**2*Es[6]*fu.mc_bracket(eps_top) - Es[6]*fu.mc_bracket(-eps_top), V0)
        s1 = strain_bottom(p_bottom)
        st1 = stress_bottom(p_bottom)
        s2 = strain_top(p_top)
        st2 = stress_top(p_top)
        s3 = 0.0
        st3 = 0.0
        disp = lg_displ[0](p_top)
        stress_file.write(str(t_i) + "\t" + str(disp) + "\t" + str(s1) + "\t" + str(st1) + "\t" + str(s2) + "\t" + str(st2) + "\t" + str(s3) + "\t" + str(st3) + "\n")
        stress_file.close()
        react_file = open(folder + "/reaction_data.txt", "a")
        forces = fe.assemble(u_form)
        react = forces[r_dof]
        react_file.write(str(t_i) + "\t" + str(disp) + "\t" + str(react) + "\n")
        react_file.close()

    # Calculate new lambda and mu for foil
    def update_foil(t_i):
        #G_foil_t = get_G(t_i*60.0/1.8/2)
        G_foil_t = get_G(t_i/2)
        G_foil.g = G_foil_t

    # Get foil stiffness for given time t_i
    def get_G(t_i):
        a_t = math.pow(10.0, -c1*(T_act - T_ref)/(c2 + T_act - T_ref))
        temp = G_inf
        n = len(G)
        temp += sum(G[k]*math.exp(-t_i/(a_t*theta[k])) for k in range(0, n))
        return temp

    # Displacement form getter
    def get_u_form():
        # Selective integration for shearlock reduction
        dx_shear = fe.dx(metadata={"quadrature_degree": 0})

        lg_test = fe.TestFunction(V)

        w_tr_a, u_tr_a, phi_tr_a = map_to_layers(u, h, False)
        w_test_a, u_test_a, phi_test_a = map_to_layers(lg_test, h, False)

        u_form_i = 0.0

        A = [b * hi for hi in h]
        print(A)
        Is = [1.0 / 12.0 * b * hi ** 3 for hi in h]

        glass_ind = 0

        for k in range(n_lay):
            # ind = 3 * k
            # ind_max = 3*n_lay

            u_tr, w_tr, phi_tr = u_tr_a[k], w_tr_a[k], phi_tr_a[k]
            u_test, w_test, phi_test = u_test_a[k], w_test_a[k], phi_test_a[k]

            if k % 2 == 0:
            # if k > 1000:
                hs = np.linspace(-0.5 * h[k], 0.5 * h[k], n_ni)
                dh = abs(hs[1] - hs[0])
                for j in range(len(hs) - 1):
                    gauss = 0.5*(hs[j] + hs[j + 1])
                    eps_z = u_tr.dx(0) + phi_tr.dx(0)*gauss
                    d_eps_z = u_test.dx(0) + phi_test.dx(0)*gauss
                    eps_z_p = fu.mc_bracket(eps_z)
                    eps_z_n = -fu.mc_bracket(-eps_z)
                    u_form_i += fe.inner((1.0 - d[glass_ind]) ** 2 * Es[k] * eps_z_p + Es[k] * eps_z_n,
                                         d_eps_z)*dh*b*fe.dx
                glass_ind += 1
            else:
                u_form_i += u_test.dx(0) * Es[k] * A[k] * u_tr.dx(0) * fe.dx
                u_form_i += phi_test.dx(0) * Es[k] * Is[k] * phi_tr.dx(0) * fe.dx

            u_form_i += (w_test.dx(0) + phi_test) * Gs[k] * A[k] * (w_tr.dx(0) + phi_tr) * dx_shear
            u_form_i -= fe.Constant(0.0)*w_test*fe.dx

        return u_form_i

    # For 11L only
    def map_to_layers(fce_i, h_i, dpcopy):
        # w1, u1, phi1, u3, phi3, ...
        u_i = [None] * 11
        w_i = [None] * 11
        phi_i = [None] * 11

        if dpcopy:
            w1_m, u1_m, p1_m, u3_m, p3_m, u5_m, p5_m, u7_m, p7_m, u9_m, p9_m, u11_m, p11_m = fce_i.split(deepcopy=True)
        else:
            w1_m, u1_m, p1_m, u3_m, p3_m, u5_m, p5_m, u7_m, p7_m, u9_m, p9_m, u11_m, p11_m = fe.split(fce_i)

        # Master us
        u_i[0] = u1_m
        u_i[2] = u3_m
        u_i[4] = u5_m
        u_i[6] = u7_m
        u_i[8] = u9_m
        u_i[10] = u11_m

        # Master and slave ws
        w_i[0] = w1_m
        w_i[1] = w1_m
        w_i[2] = w1_m
        w_i[3] = w1_m
        w_i[4] = w1_m
        w_i[5] = w1_m
        w_i[6] = w1_m
        w_i[7] = w1_m
        w_i[8] = w1_m
        w_i[9] = w1_m
        w_i[10] = w1_m

        # Master phis
        phi_i[0] = p1_m
        phi_i[2] = p3_m
        phi_i[4] = p5_m
        phi_i[6] = p7_m
        phi_i[8] = p9_m
        phi_i[10] = p11_m

        # Slave us and phis
        u_i[1] = 0.25*h_i[0]*phi_i[0] - 0.25*h_i[2]*phi_i[2] + 0.5*u_i[2] + 0.5*u_i[0]
        u_i[3] = 0.25*h_i[2]*phi_i[2] - 0.25*h_i[4]*phi_i[4] + 0.5*u_i[4] + 0.5*u_i[2]
        u_i[5] = 0.25*h_i[4]*phi_i[4] - 0.25*h_i[6]*phi_i[6] + 0.5*u_i[6] + 0.5*u_i[4]
        u_i[7] = 0.25*h_i[6]*phi_i[6] - 0.25*h_i[8]*phi_i[8] + 0.5*u_i[8] + 0.5*u_i[6]
        u_i[9] = 0.25*h_i[8]*phi_i[8] - 0.25*h_i[10]*phi_i[10] + 0.5*u_i[10] + 0.5*u_i[8]

        phi_i[1] = -0.5*phi_i[0]*h_i[0]/h_i[1] - 0.5*phi_i[2]*h_i[2]/h_i[1] + u_i[2]/h_i[1] - u_i[0]/h_i[1]
        phi_i[3] = -0.5*phi_i[2]*h_i[2]/h_i[3] - 0.5*phi_i[4]*h_i[4]/h_i[3] + u_i[4]/h_i[3] - u_i[2]/h_i[3]
        phi_i[5] = -0.5*phi_i[4]*h_i[4]/h_i[5] - 0.5*phi_i[6]*h_i[6]/h_i[5] + u_i[6]/h_i[5] - u_i[4]/h_i[5]
        phi_i[7] = -0.5*phi_i[6]*h_i[6]/h_i[7] - 0.5*phi_i[8]*h_i[8]/h_i[7] + u_i[8]/h_i[7] - u_i[6]/h_i[7]
        phi_i[9] = -0.5*phi_i[8]*h_i[8]/h_i[9] - 0.5*phi_i[10]*h_i[10]/h_i[9] + u_i[10]/h_i[9] - u_i[8]/h_i[9]

        return w_i, u_i, phi_i

    # Return formulation for damage
    def get_d_form():
        d_i = fe.split(d)
        # d_tr = fe.TrialFunctions(self.W)
        d_test = fe.TestFunctions(W)

        d_form_i = 0.0
        for k in range(glass_num):
            d_form_i += -2 * get_energy_active(u, 2 * k) * fe.inner(1.0 - d_i[k], d_test[k]) * fe.dx
            d_form_i += b * h[2 * k] * 3.0 / 8.0 * Gc_glass[k] * (1.0 / lc * d_test[k] + 2 * lc * fe.inner(fe.grad(d_i[k]), fe.grad(d_test[k]))) * fe.dx

        return d_form_i

    def get_energy_active(x, l_num):
        w_tr, u_tr, phi_tr = map_to_layers(x, h, False)
        u_, w_, phi_ = u_tr[l_num], w_tr[l_num], phi_tr[l_num]
        h_i = h[l_num]
        E = Es[l_num]
        eps_i_1 = u_.dx(0) + phi_.dx(0)*0.5*h_i
        eps_i_2 = u_.dx(0) - phi_.dx(0)*0.5*h_i
        eps_max = fu.max_fce(eps_i_1, eps_i_2)
        en = 0.5*fu.mc_bracket(eps_max)**2*E*h_i*b
        return en

    def get_sigma_test(x, l_num):
        lg_fce = fe.split(x)
        u_, w_, phi_ = lg_fce[3 * l_num], lg_fce[3 * l_num + 1], lg_fce[3 * l_num + 2]
        h_i = h[l_num]
        E = Es[l_num]
        eps_i_1 = u_.dx(0) + phi_.dx(0)*0.5*h_i
        eps_i_2 = u_.dx(0) - phi_.dx(0)*0.5*h_i
        en1 = fu.mc_bracket(eps_i_1)*E
        en2 = fu.mc_bracket(eps_i_2) * E
        return en1, en2

    def get_stress_in_point(p, u_i, d_i):
        ind, h_l = get_layer_number(p, h)

        w_, u_, phi_ = map_to_layers(u_i, h, True)
        u_i = u_[ind]
        phi_i = phi_[ind]
        eps_i = u_i.dx(0) + phi_i.dx(0)*h_l
        strain = fu.local_project(eps_i, V0)
        if ind == 0:
            d_ = d_i[0]
        elif ind == 2:
            d_ = d_i[1]
        elif ind == 4:
            d_ = d_i[2]
        elif ind == 6:
            d_ = d_i[3]
        elif ind == 8:
            d_ = d_i[4]
        else:
            d_ = 0.0
        # stress = fu.local_project(E[ind]*eps_i, self.V0)
        stress = fu.local_project((1.0 - d_)**2*Es[ind]*fu.mc_bracket(eps_i) - Es[ind]*fu.mc_bracket(-eps_i), V0)
        return strain(p.x()), stress(p.x())

    # Only for 7 layered
    def get_layer_number(p_i, hs_i):
        print(p_i.z(), sum(hs_i))
        if p_i.z() >= -0.5 * hs_i[0]:
            if p_i.z() <= 0.5 * hs_i[0]:
                return 0, p_i.z()
            elif p_i.z() < 0.5 * hs_i[0] + hs_i[1]:
                return 1, p_i.z() - 0.5*hs_i[0] - 0.5*hs_i[1]
            elif p_i.z() <= 0.5 * hs_i[0] + hs_i[1] + hs_i[2]:
                return 2, p_i.z() - 0.5*hs_i[0] - hs_i[1] - 0.5*hs_i[2]
            elif p_i.z() < 0.5 * hs_i[0] + hs_i[1] + hs_i[2] + hs_i[3]:
                return 3, p_i.z() - 0.5*hs_i[0] - hs_i[1] - hs_i[2] - 0.5*hs_i[3]
            elif p_i.z() <= 0.5 * hs_i[0] + hs_i[1] + hs_i[2] + hs_i[3] + hs_i[4]:
                return 4, p_i.z() - 0.5*hs_i[0] - hs_i[1] - hs_i[2] - hs_i[3] - 0.5*hs_i[4]
            elif p_i.z() < 0.5 * hs_i[0] + hs_i[1] + hs_i[2] + hs_i[3] + hs_i[4] + hs_i[5]:
                return 5, p_i.z() - 0.5*hs_i[0] - hs_i[1] - hs_i[2] - hs_i[3] - hs_i[4] - 0.5*hs_i[5]
            elif p_i.z() <= 0.5 * hs_i[0] + hs_i[1] + hs_i[2] + hs_i[3] + hs_i[4] + hs_i[5] + hs_i[6]:
                return 6, p_i.z() - 0.5*hs_i[0] - hs_i[1] - hs_i[2] - hs_i[3] - hs_i[4] - hs_i[5] - 0.5*hs_i[6]
        return -1

    # --------------------
    # Parameters of task
    # --------------------
    # Material parameters
    E_glass = 70.0e9  # Young's modulus
    nu_glass = 0.22  # Poisson ratio
    G = vis_mat.Gs
    theta = vis_mat.thetas
    nu_vis = vis_mat.nu
    G_inf = vis_mat.Ginf
    c1 = vis_mat.c1
    c2 = vis_mat.c2
    T_ref = vis_mat.Tref
    T_act = vis_mat.Tact

    # Geometry parameters
    h = [0.006, 0.00152, 0.006, 0.00076, 0.006, 0.00076, 0.006, 0.00076, 0.006, 0.00152, 0.006]
    h_tot = sum(h)  # Total thickness of lg beam
    l_x = 1.1  # Length and thickness of glass beam
    l_off = 0.05  # Support offset
    l_0 = 0.2  # Pitch of load points
    b = 0.36
    n_lay = 11
    glass_num = 6
    n_ni = 40

    p_bottom = fe.Point((0.5*l_x))
    p_top = fe.Point((0.5*l_x))
    p_react = fe.Point((0.5 * l_x - 0.5 * l_0))

    # First initiation of G foil. The values are recalculated in each time step.
    G_foil = fe.Expression("g", g=0.0, degree=0)  # G value
    G_glass = E_glass/2/(1+nu_glass)
    E_foil = 2*G_foil*(1+nu_vis)

    Gs = n_lay*[G_glass]
    Es = n_lay*[E_glass]
    for i in range(len(Gs)):
        if i % 2 != 0:
            Gs[i] = G_foil
            Es[i] = E_foil

    print(Gs[1])

    # --------------------
    # Define geometry
    # --------------------
    mesh = prepare_mesh()

    # Fracture parameters
    h_min = mesh.hmin()
    lc = 2*h_min

    ft = []
    Gc_glass = []
    for i in range(glass_num):
        ft_i = fe.Constant(fts[i])
        ft.append(ft_i)
        Gc_glass.append(lc*8.0/3.0*ft_i**2/E_glass)

    files_strength = [fe.File(folder + "/strength_field_{}.pvd".format(k)) for k in range(glass_num)]
    [files_strength[k] << fe.interpolate(ft[k], fe.FunctionSpace(mesh, "CG", 1)) for k in range(glass_num)]

    fig, axs = plt.subplots(glass_num, 1)
    x = mesh.coordinates()
    for i in range(glass_num):
        axs[i].plot(x, fe.interpolate(ft[i], fe.FunctionSpace(mesh, "CG", 1)).vector()[:]/1.0e6)
        plt.tight_layout()
        plt.xlabel("x coord. [m]")
        plt.ylabel("f_t [MPa]")
    plt.show()

    fe.plot(mesh)
    plt.show()

    # --------------------
    # Function spaces
    # --------------------
    p1 = fe.FiniteElement("P", fe.interval, 1)
    elems_num = glass_num*2 + 1
    element = fe.MixedElement(elems_num*[p1])
    V = fe.FunctionSpace(mesh, element)
    element_d = fe.MixedElement(glass_num*[p1])
    W = fe.FunctionSpace(mesh, element_d)
    V0 = fe.FunctionSpace(mesh, "DG", 0)
    W0 = fe.FunctionSpace(mesh, "CG", 1)

    # r_dof = fu.find_dof(p_react, 1, V)
    r_dof = fu.find_dof_beam(p_react, 0, V)

    print(V.dim())

    # --------------------
    # Boundary conditions
    # --------------------
    # Prepare indices
    ind_left_point = 0
    ind_left_load_point = 0
    u_d = fe.Expression("-t*lv/60.0/1000.0", t=0.0, lv=load_velocity, degree=0)  # Prescribed displacement
    BC_u = []
    BC_u.append(fe.DirichletBC(V.sub(ind_left_point), 0.0, left_point, method="pointwise"))
    BC_u.append(fe.DirichletBC(V.sub(ind_left_load_point), u_d, left_load_point, method="pointwise"))
    for i in range(glass_num):
        ind1 = i*2 + 1
        ind2 = i*2 + 2
        BC_u.append(fe.DirichletBC(V.sub(ind1), 0.0, symmetry_axis))
        BC_u.append(fe.DirichletBC(V.sub(ind2), 0.0, symmetry_axis))
    BC_d = []

    # --------------------
    # Initialization
    # --------------------
    u = fe.Function(V)
    u_old = fe.Function(V)
    du = fe.Function(V)
    d = fe.Function(W)
    d_old = fe.Function(W)
    dd = fe.Function(W)
    d_min = fe.Function(W)
    en_prev = 0.0

    file_u, file_d = prepare_files(folder, params, glass_num, n_lay, W0)

    # --------------------
    # Forms
    # --------------------

    u_form = get_u_form()

    d_form = get_d_form()

    solve_staggered()


# Solver parameters
prms = fu.SolverParams()
prms.max_iter = 100
prms.tol_nm = 1.0e-12
prms.tol_sl = 1.0e-6
prms.conv_crit = "rel_inc"

# Viscoelasticmaterial - PVB
pvb_mat = fu.ViscoelasticMaterial()
pvb_mat.Gs = 1.0e3*np.array([1782124.2, 519208.7, 546176.8, 216893.2, 13618.3, 4988.3, 1663.8, 587.2, 258.0, 63.8, 168.4])
pvb_mat.thetas = np.array([1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5])
pvb_mat.nu = 0.49
pvb_mat.Ginf = 232.26e3
pvb_mat.c1 = 8.635
pvb_mat.c2 = 42.422
pvb_mat.Tref = 20.0
pvb_mat.Tact = 23.88

# Loading velocity in mm/min
load_vel = 1.0

# Glass strength
f_t = 200.0e9

# Timespaces
time_space = fu.make_time_space([0.1, 1000.0], [3.0])

# Solution
#glass_9l_lg_solver(time_space, [f_t,f_t,f_t,f_t,f_t,f_t], "Solutions/11L_temp_23", prms, pvb_mat, load_vel)
pvb_mat.Tact = 27.97
#glass_9l_lg_solver(time_space, [f_t,f_t,f_t,f_t,f_t,f_t], "Solutions/11L_temp_28", prms, pvb_mat, load_vel)

# Solution
ext_file_stress_23 = np.loadtxt("Solutions/11L_temp_23/stress_data.txt", skiprows=3)
ext_file_stress_28 = np.loadtxt("Solutions/11L_temp_28/stress_data.txt", skiprows=3)
plt.plot(ext_file_stress_23[:, 0], ext_file_stress_23[:, 3] / 1.0e6, label="T=23.88")
plt.plot(ext_file_stress_28[:, 0], ext_file_stress_28[:, 3] / 1.0e6, label="T=27.97")
plt.xlabel("Prescribed displacement [mm]")
plt.ylabel("Stress on bottom [MPa]")
plt.legend()
plt.show()

ext_file_react_23 = np.loadtxt("Solutions/11L_temp_23/reaction_data.txt", skiprows=3)
ext_file_react_28 = np.loadtxt("Solutions/11L_temp_28/reaction_data.txt", skiprows=3)
plt.plot(ext_file_react_23[:, 0], 2*ext_file_react_23[:, 2] / 1.0e3, label="T=23.88")
plt.plot(ext_file_react_28[:, 0], 2*ext_file_react_28[:, 2] / 1.0e3, label="T=27.97")
plt.xlabel("Prescribed displacement [mm]")
plt.ylabel("Reaction [kN]")
plt.legend()
plt.show()

ext_file_react_23_7 = np.loadtxt("Solutions/7L_temp_23/reaction_data.txt", skiprows=3)
ext_file_react_28_7 = np.loadtxt("Solutions/7L_temp_28/reaction_data.txt", skiprows=3)
ext_file_react_23_9 = np.loadtxt("Solutions/9L_temp_23/reaction_data.txt", skiprows=3)
ext_file_react_28_9 = np.loadtxt("Solutions/9L_temp_28/reaction_data.txt", skiprows=3)
ext_file_react_23_11 = np.loadtxt("Solutions/11L_temp_23/reaction_data.txt", skiprows=3)
ext_file_react_28_11 = np.loadtxt("Solutions/11L_temp_28/reaction_data.txt", skiprows=3)
plt.plot(ext_file_react_23_7[:, 0]/60, 2*ext_file_react_23_7[:, 2] / 1.0e3, c="r", label="7L, T=23.88 C")
plt.plot(ext_file_react_28_7[:, 0]/60, 2*ext_file_react_28_7[:, 2] / 1.0e3, "--", c="r", label="7L, T=27.97 C")
plt.plot(ext_file_react_23_9[:, 0]/60, 2*ext_file_react_23_9[:, 2] / 1.0e3, c="b", label="9L, T=23.88 C")
plt.plot(ext_file_react_28_9[:, 0]/60, 2*ext_file_react_28_9[:, 2] / 1.0e3, "--", c="b", label="9L, T=27.97 C")
plt.plot(ext_file_react_23_11[:, 0]/60, 2*ext_file_react_23_11[:, 2] / 1.0e3, c="g", label="11L, T=23.88 C")
plt.plot(ext_file_react_28_11[:, 0]/60, 2*ext_file_react_28_11[:, 2] / 1.0e3, "--", c="g", label="11L, T=27.97 C")
ind = 33
print(ext_file_react_23_11[ind, 2]/ext_file_react_28_11[ind, 2])
print(ext_file_react_23_9[-1, 2]/ext_file_react_28_9[-1, 2])
print(ext_file_react_23_11[-1, 2])
plt.text(360/60, -1.25, "12.8 %", c="r", fontsize=12)
plt.text(370/60, -1.8, "14 %", c="b", fontsize=12)
plt.text(340/60, -3, "15 %", c="g", fontsize=12)
#plt.xlim(0, 100)
#plt.ylim(-1, 0)
plt.xlabel("Prescribed displacement [mm]")
plt.ylabel("Reaction [kN]")
plt.legend()
plt.show()