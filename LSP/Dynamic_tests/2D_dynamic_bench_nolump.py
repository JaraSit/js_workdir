# -------------------------
# 2D_dynamic_bench_nolump.py
# -------------------------

# -------------------------
# Description:
# - Benchmark case for openfoam
# - No lumping used
#
# Last edit: 27.04. 2022
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt
import numpy as np
import time


# --------------------
# Functions and classes
# --------------------
def impact(x, on_boundary):
    return on_boundary and fe.near(x[1], 0.0)


# Strain function
def epsilon(u_i):
    return 0.5*(fe.nabla_grad(u_i) + fe.nabla_grad(u_i).T)


# Stress function
def sigma(u_i):
    return lmbda*fe.div(u_i)*fe.Identity(2) + 2*mu*epsilon(u_i)


# --------------------
# Parameters
# --------------------
E, nu = 1.0, 0.1  # Young's modulus and Poisson's ratio
l_x, l_y = 0.5, 5.0  # Domain dimensions
rho = 1.0  # Density
t_e, p_m = 3.0, 10.0  # Parameters of pulse

# Time-stepping
t_start = 0.0  # start time
t_end = 15  # end time
t_steps = 3000  # number of time steps

# Numerics
n_x, n_y = 10, 400
el_type = "rect"
save_rate = 10

# Lame's constants
lmbda = E*nu/(1+nu)/(1-2*nu)
mu = E/2/(1+nu)

t, dt = np.linspace(t_start, t_end, t_steps, retstep=True)
dt = float(dt)  # time step needs to be converted from class 'numpy.float64' to class 'float' for the .assign() method to work (see below)

# --------------------
# Geometry
# --------------------
# Mesh
elem_type = fe.CellType.Type.quadrilateral
if el_type == "rect":
    elem_type = fe.CellType.Type.quadrilateral
elif el_type == "tri":
    elem_type = fe.CellType.Type.triangle
mesh = fe.RectangleMesh.create([fe.Point(0.0, 0.0), fe.Point(l_x, l_y)], [n_x, n_y], elem_type)
#mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(l_x, l_y), n_x, n_y)

#fe.plot(mesh)
#plt.show()

# --------------------
# Function spaces
# --------------------
V = fe.VectorFunctionSpace(mesh, "CG", 1)
u_tr = fe.TrialFunction(V)
u_test = fe.TestFunction(V)

# --------------------
# Boundary conditions
# --------------------
u_f = fe.Expression(" t < 0.5*t_e ? p_m/t_e*2*t : ( t < t_e ? p_m - p_m/t_e*2*(t-0.5*t_e) : 0)", t=0.0, t_e=t_e, p_m=p_m, degree=0)


u_ff = []
# Test
for t_i in t:
    u_f.t = t_i
    u_ff.append(u_f(0.0, 0.0))

plt.plot(t, u_ff, "x")
plt.show()

# Definition of Neumann condition domain
boundaries = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
boundaries.set_all(0)

impact_area = fe.AutoSubDomain(lambda x: fe.near(x[1], 0.0))
impact_area.mark(boundaries, 1)
ds = fe.ds(subdomain_data=boundaries)

filei = fe.File("Solutions/2D_dynamic_bench/mesh.pvd")
filei << boundaries

# --------------------
# Initialization
# --------------------
u = fe.Function(V)
u_old = fe.Function(V)
u_old2 = fe.Function(V)

file = fe.XDMFFile("Solutions/2D_dynamic_bench/u.xdmf")  # XDMF file

# --------------------
# Matrices
# --------------------
A_form = fe.inner(sigma(u_old), epsilon(u_test))*fe.dx + rho/dt**2*fe.dot(u_tr - 2*u_old + u_old2, u_test)*fe.dx + u_f*u_test[1]*ds(1)
E_int = fe.inner(sigma(u_old), epsilon(u_old))*fe.dx
E_kin = rho/dt**2*fe.dot(u - u_old, u - u_old)*fe.dx
#M_form = rho*fe.dot(u_test, u_tr)*fe.dx
#M_form_quad = rho*fe.dot(u_test, u_tr)*fe.dx(scheme="vertex", metadata={"degree": 1, "representation": "quadrature"})
#K_form = fe.inner(sigma(u_tr), epsilon(u_test))*fe.dx
#F_form = fe.inner(fe.Constant((1.0, 1.0)), u_test)*ds(1)
#F_form = fe.Constant(1.0)*u_test[1]*ds(1)
#F_vector = fe.assemble(F_form)
#print(F_vector.get_local())
#K_matrix = fe.assemble(K_form)
#M_lumped = fe.assemble(M_form_quad)
#M_fce = fe.Function(V)
#M_lumped_vector = M_fce.vector()
#M_lumped.get_diagonal(M_lumped_vector)

# --------------------
# Time loop
# --------------------
tt = []
eint = []
ekin = []
etot = []
# Main time loop
saveindex = save_rate
for ti in t:
    u_f.t = ti
    #print(u_f(0.0, 0.0))

    # Update of RHS
    #w_vector = -dt**2*K_matrix*u_old.vector() - u_f(0.0, 0.0)*dt**2*F_vector - M_lumped*(u_old2.vector() - 2.0 * u_old.vector())

    #iMw = fe.Function(V)
    #iMw.vector().set_local(w_vector.get_local() / M_lumped_vector.get_local())
    #u.assign(iMw)
    fe.solve(fe.lhs(A_form) == fe.rhs(A_form), u, [])

    file.write(u, ti)

    if saveindex >= save_rate:
        tt.append(ti)
        eint_i = fe.assemble(E_int)
        ekin_i = fe.assemble(E_kin)
        eint.append(eint_i)
        ekin.append(ekin_i)
        etot.append(eint_i + ekin_i)

        saveindex = 0
    else:
        saveindex += 1

    u_old2.assign(u_old)
    u_old.assign(u)

    #t += dt

file.close()

plt.plot(tt, eint, label="E_int")
plt.plot(tt, ekin, label="E_kin")
plt.plot(tt, etot, label="E_tot")
plt.legend()
plt.xlabel("Time")
plt.ylabel("Energy")
plt.show()
