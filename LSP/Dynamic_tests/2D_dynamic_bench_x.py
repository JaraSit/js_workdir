# -------------------------
# 2D_dynamic_bench_x.py
# -------------------------

# -------------------------
# Description:
# - Benchmark case for openfoam
# - Implemented in fenicsx
#
# Last edit: 27.04. 2022
# -------------------------

import dolfinx
import matplotlib.pyplot as plt
import numpy as np
import time
import ufl
from mpi4py import MPI


# --------------------
# Functions and classes
# --------------------
def impact_face(x):
    return np.isclose(x[1], l_y)


# Strain function
def epsilon(v):
    return ufl.sym(ufl.grad(v))


# Stress function
def sigma(v):
    return lmbda*ufl.div(v)*ufl.Identity(v.geometric_dimension()) + 2*mu*epsilon(v)


# --------------------
# Parameters
# --------------------
E, nu = 0.02e9, 0.1  # Young's modulus and Poisson's ratio
l_x, l_y = 5.0, 5.0  # Domain dimensions
rho = 200.0  # Density

# Time-stepping
t_start = 0.0  # start time
t_end = 1.0e-1  # end time
t_steps = 100  # number of time steps

# Numerics
n_x, n_y = 100, 100
el_type = "rect"

# Lame's constants
lmbda = E*nu/(1+nu)/(1-2*nu)
mu = E/2/(1+nu)

t, dt = np.linspace(t_start, t_end, t_steps, retstep=True)
dt = float(dt)  # time step needs to be converted from class 'numpy.float64' to class 'float' for the .assign() method to work (see below)

# --------------------
# Geometry
# --------------------
if el_type == "rect":
    mesh = dolfinx.RectangleMesh(MPI.COMM_WORLD, [np.array([0.0, 0.0, 0.0]), np.array([l_x, l_y, 0.0])], [n_x, n_y],
                                 cell_type=CellType.quadrilateral)
    p_in = 0.5 / t_el
    print(p_in)
else:
    raise Exception("Only *rect* element type implemented!")
mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(l_x, l_y), n_x, n_y)

fe.plot(mesh)
plt.show()

# --------------------
# Function spaces
# --------------------
V = fe.VectorFunctionSpace(mesh, "CG", 1)
u_tr = fe.TrialFunction(V)
u_test = fe.TestFunction(V)

# --------------------
# Boundary conditions
# --------------------
u_D = fe.Expression(" t < 1.0e-2 ? -100*t : -1.0", t=0.0, degree=0)

bc1 = fe.DirichletBC(V, fe.Constant((0.0, 0.0)), bottom)
bc2 = fe.DirichletBC(V.sub(1), u_D, indent_area)
bc = [bc1, bc2]

# --------------------
# Initialization
# --------------------
u = fe.Function(V)
u_bar = fe.Function(V)
du = fe.Function(V)
ddu = fe.Function(V)
ddu_old = fe.Function(V)

file = fe.XDMFFile("FEniCS_explicit_output.xdmf")  # XDMF file

# --------------------
# Weak form
# --------------------
A_form = fe.inner(sigma(u_tr), epsilon(u_test))*fe.dx + 4*rho/(dt*dt)*fe.dot(u_tr - u_bar, u_test)*fe.dx

# --------------------
# Time loop
# --------------------
for ti in t:
    u_D.t = ti
    u_bar.assign(u + dt*du + 0.25*dt*dt*ddu)

    #A, b = fe.assemble_system(fe.lhs(A_form), fe.rhs(A_form), bc)
    #fe.solve(A, u.vector(), b, "cg", "jacobi")
    fe.solve(fe.lhs(A_form) == fe.rhs(A_form), u, bc)

    ddu_old.assign(ddu)
    ddu.assign(4/(dt*dt)*(u - u_bar))
    du.assign(du + 0.5*dt*(ddu + ddu_old))

    file.write(u, ti)

    #print(ti)
file.close()