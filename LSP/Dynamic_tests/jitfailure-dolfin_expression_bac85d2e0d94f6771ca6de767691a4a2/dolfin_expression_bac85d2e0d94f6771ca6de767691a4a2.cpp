
// Based on https://gcc.gnu.org/wiki/Visibility
#if defined _WIN32 || defined __CYGWIN__
    #ifdef __GNUC__
        #define DLL_EXPORT __attribute__ ((dllexport))
    #else
        #define DLL_EXPORT __declspec(dllexport)
    #endif
#else
    #define DLL_EXPORT __attribute__ ((visibility ("default")))
#endif

#include <dolfin/function/Expression.h>
#include <dolfin/math/basic.h>
#include <Eigen/Dense>


// cmath functions
using std::cos;
using std::sin;
using std::tan;
using std::acos;
using std::asin;
using std::atan;
using std::atan2;
using std::cosh;
using std::sinh;
using std::tanh;
using std::exp;
using std::frexp;
using std::ldexp;
using std::log;
using std::log10;
using std::modf;
using std::pow;
using std::sqrt;
using std::ceil;
using std::fabs;
using std::floor;
using std::fmod;
using std::max;
using std::min;

const double pi = DOLFIN_PI;


namespace dolfin
{
  class dolfin_expression_bac85d2e0d94f6771ca6de767691a4a2 : public Expression
  {
     public:
       double t;
double t_e;
double p_m;


       dolfin_expression_bac85d2e0d94f6771ca6de767691a4a2()
       {
            
       }

       void eval(Eigen::Ref<Eigen::VectorXd> values, Eigen::Ref<const Eigen::VectorXd> x) const override
       {
          values[0] =  t < 0.5*t_e ? p_m/t_e*2*t : ( t < t_e ? p_m - p_m/t_e*2*(t-0.5*t_e)) : 0;

       }

       void set_property(std::string name, double _value) override
       {
          if (name == "t") { t = _value; return; }          if (name == "t_e") { t_e = _value; return; }          if (name == "p_m") { p_m = _value; return; }
       throw std::runtime_error("No such property");
       }

       double get_property(std::string name) const override
       {
          if (name == "t") return t;          if (name == "t_e") return t_e;          if (name == "p_m") return p_m;
       throw std::runtime_error("No such property");
       return 0.0;
       }

       void set_generic_function(std::string name, std::shared_ptr<dolfin::GenericFunction> _value) override
       {

       throw std::runtime_error("No such property");
       }

       std::shared_ptr<dolfin::GenericFunction> get_generic_function(std::string name) const override
       {

       throw std::runtime_error("No such property");
       }

  };
}

extern "C" DLL_EXPORT dolfin::Expression * create_dolfin_expression_bac85d2e0d94f6771ca6de767691a4a2()
{
  return new dolfin::dolfin_expression_bac85d2e0d94f6771ca6de767691a4a2;
}

