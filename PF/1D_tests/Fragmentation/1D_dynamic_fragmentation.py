# -------------------------
# 1D_dynamic_fragmentation.py
# -------------------------

# -------------------------
# Description:
# - 1D phase field dynamic fragmentation test
#
# Last edit: 19.04. 2022
# -------------------------

import numpy as np
import fenics as fe
import matplotlib.pyplot as plt
import js_workdir.utils.forms as fo
import math as m


def left_end(x):
    return fe.near(x[0], 0.0)


def right_end(x):
    return fe.near(x[0], l)


def middle_point(x):
    return fe.near(x[0], l / 2)


# Sub domain for Periodic boundary condition
class PeriodicBoundary(fe.SubDomain):

    # Left boundary is "target domain" G
    def inside(self, x, on_boundary):
        return bool(x[0] < fe.DOLFIN_EPS and x[0] > -fe.DOLFIN_EPS and on_boundary)

    # Map right boundary (H) to left boundary (G)
    def map(self, x, y):
        y[0] = x[0] - 1.0


# --------------------
# Parameters
# --------------------
n = 200
l = 1.0
A = 1.0
#E = 400.0e9
#rho = 3900.0
#Kc = 5.0e6
#ft = 400e6
E = 1
#Kc = 0.1
ft = 1
rho = 1
#lc = 3/(4*m.sqrt(2))*(Kc/ft)**2
#Gc = Kc**2/E

lc = 0.01
Gc = 4.0

# Time parameters
t = 0.0
t_end = 60.0
dt = 0.1

# Iteration parameters
iterr = 1
error = 1.0
toll = 1.0e-4
maxiter = 30

# --------------------
# Define geometry
# --------------------
mesh = fe.IntervalMesh(n, 0.0, l)
fe.plot(mesh, "Mesh")
plt.show()

# --------------------
# Define spaces
# --------------------
# Create periodic boundary condition
pbc = PeriodicBoundary()

V = fe.FunctionSpace(mesh, "CG", 1, constrained_domain=pbc)
V2 = fe.FunctionSpace(mesh, "CG", 1)
W = fe.FunctionSpace(mesh, "DG", 0)
u_tr = fe.TrialFunction(V)
u_test = fe.TestFunction(V)
d_tr = fe.TrialFunction(V)
d_test = fe.TestFunction(V)

# --------------------
# Boundary conditions
# --------------------
u_D = fe.Expression("0.5*t", t=0.0, degree=0)
BC_u_1 = fe.DirichletBC(V, fe.Constant(0.0), left_end)
BC_u_2 = fe.DirichletBC(V, u_D, right_end)
BC_u = [BC_u_1, BC_u_2]
BC_s = []

# --------------------
# Initialization
# --------------------
u = fe.Function(V)
u_old = fe.Function(V)
u_bar = fe.Function(V)  # Auxiliary displacement function in time t_i
du = fe.Function(V)
ddu = fe.Function(V)
ddu_old = fe.Function(V)
d = fe.Function(V2)
d_old = fe.Function(V2)

#E = fe.Expression("1.0 - 0.1*near(x[0], 0.5, 0.001)", degree=1)
fe.plot(fe.project(E, V))
plt.show()

# --------------------
# Variational problem
# --------------------
#E_du = fo.get_1D_form_staggered_u("b", d, V, E, A)
#E_ds = fo.get_1D_form_staggered_d("b", u, V, E, A, lc, Gc)

E_du = (1 - d)**2*E*A*fe.inner(fe.grad(u_test), fe.grad(u_tr))*fe.dx+ 4*rho/(dt*dt)*fe.dot(u_tr - u_bar, u_test)*fe.dx
E_ds = -0.5*E*A*fe.inner(fe.grad(u), fe.grad(u))*fe.inner(1.0 - d_tr, d_test)*fe.dx + Gc*(
                    1.0/lc*fe.inner(d_tr, d_test) +
                    lc*fe.inner(fe.grad(d_tr), fe.grad(d_test)))*fe.dx

force = []
time = []

while t < t_end:
    u_D.t = t
    print(u_D.t)
    u_bar.assign(u + dt*du + 0.25*dt*dt*ddu)

    fe.solve(fe.lhs(E_du) == fe.rhs(E_du), u, BC_u)

    fe.solve(fe.lhs(E_ds) == fe.rhs(E_ds), d, BC_s)

    ddu_old.assign(ddu)
    ddu.assign(4/(dt*dt)*(u - u_bar))
    du.assign(du + 0.5*dt*(ddu + ddu_old))

    time.append(t)
    sigma = fe.project((1.0-d)**2*E*A*u.dx(0), W)
    force.append(sigma.vector()[n-1])

    t += dt

plt.plot(time, force)
plt.xlabel("t")
plt.ylabel("reaction")
plt.figure()
fe.plot(u)
plt.xlabel("x")
plt.ylabel("u(x)")
plt.figure()
fe.plot(d)
plt.xlabel("x")
plt.ylabel("d(x)")
plt.show()
