# -------------------------
# plate_explicit_explosion_test.py
# -------------------------

# -------------------------
# Description:
# - Explicit dynamic with damage
# - Mindlin plate theory
# - Soft device loading
# - Decomposition through thickness
#
# Last edit: 04.04. 2022
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt
import time
import numpy as np
import math
#import lumping_scheme_quad
import os
import js_workdir.utils.forms as fm
import js_workdir.utils.functions as fu


def right_edge(x, on_boundary):
    return on_boundary and fe.near(x[0], 0.5 * L)


def top_edge(x, on_boundary):
    return on_boundary and fe.near(x[1], 0.5 * B)


def edge(x, on_boundary):
    return on_boundary and (fe.near(x[1], 0.0) or fe.near(x[0], 0.0))


def explicit_solver(folder, saving, save_rate, point_type, el_type_i, dec_type_i):
    # Define spaces
    p1_element = fe.FiniteElement("P", mesh.ufl_cell(), deg)
    v1_element = fe.VectorElement("P", mesh.ufl_cell(), deg)
    element = fe.MixedElement([p1_element, v1_element, v1_element])
    V = fe.FunctionSpace(mesh, element)
    V_sca = fe.FunctionSpace(mesh, p1_element)
    V_vec = fe.FunctionSpace(mesh, v1_element)
    W = fe.FunctionSpace(mesh, "CG", 1)
    #W0 = fe.FunctionSpace(mesh, "CG", 1)
    print("Number of unknowns: " + str(V.dim()))
    impactor = []
    tt = []

    # Displacement functions
    X = fe.Function(V)  # Displacement in time t_i
    X_old = fe.Function(V)  # Displacement in time t_{i-1}
    X_old2 = fe.Function(V)  # Displacement in time t_{i-2}
    #X_d_old = fe.Function(V)
    d = fe.Function(W)
    #hist = fe.Function(W0)

    # Initial velocity
    #fa = fe.FunctionAssigner(V, [V_sca, V_vec, V_vec, V_rea])
    #s_init = fe.interpolate(fe.Constant(0.0), V_sca)
    #v_init = fe.interpolate(fe.Constant((0.0, 0.0)), V_vec)
    #r_init = fe.interpolate(fe.Constant(-dt * v0), V_rea)
    #fa.assign(X_old2, [s_init, v_init, v_init, r_init])
    #threshold = fe.interpolate(fe.Constant(0.5), W)

    # Point distribution
    #f_point = fe.Expression("abs(x[0] - 0.5*L) < tol & abs(x[1] - 0.5*B) < tol ? 1.0 : 0.0", tol=0.001, L=L, B=B,
    #                        degree=1)
    #f_fce_vect = fe.interpolate(f_point, V_sca)
    #f_fce_real = fe.interpolate(fe.Constant(1 / (0.25 * L * B)), V_rea)

    # fe.plot(f_fce_vect)
    #file_hit = fe.File("hit_fce.pvd")
    #file_hit << f_fce_vect
    # plt.show()

    #w_test, u_test, phi_test = fe.TestFunctions(V)
    #f_dest_form = fe.dot(f_fce_vect, w_test) * fe.dx
    #f_dest_form_2 = fe.dot(f_fce_real, imp_test) * fe.dx
    #a_loc_vec_2 = fe.assemble(f_dest_form)
    #a_loc_vec_2[:] = -a_loc_vec_2[:] / sum(a_loc_vec_2[:])
    #a_loc_real = fe.assemble(f_dest_form_2)
    #index_real = np.nonzero(a_loc_real[:])[0]
    #index_hit = np.argmin(a_loc_vec_2[:])
    # print(aaa)
    # print(a_loc_vec_2[aaa])
    # p2.apply(a_loc_vec_2)

    #if point_type == "point":
    #    a_loc_vec = a_loc_vec_1
    #elif point_type == "dist":
    #    a_loc_vec = a_loc_vec_2 + a_loc_real
    #else:
    #    raise Exception("Only point/dist point type implemented!")

    # Dirichlet boundary conditions
    # w1, u1, phi1, r
    bc1 = fe.DirichletBC(V.sub(2).sub(1), fe.Constant(0.0), right_edge)
    bc2 = fe.DirichletBC(V.sub(2).sub(0), fe.Constant(0.0), top_edge)
    bc3 = fe.DirichletBC(V.sub(1).sub(0), fe.Constant(0.0), right_edge)
    bc4 = fe.DirichletBC(V.sub(1).sub(1), fe.Constant(0.0), top_edge)
    bc5 = fe.DirichletBC(V.sub(0), fe.Constant(0.0), edge)
    bc = [bc1, bc2, bc3, bc4, bc5]

    # File with solution
    file = fe.XDMFFile(folder + "/displ_w.xdmf")
    file.parameters["flush_output"] = True
    file_u = fe.XDMFFile(folder + "/displ_u.xdmf")
    file_u.parameters["flush_output"] = True
    file_d = fe.XDMFFile(folder + "/damage_d.xdmf")
    file_d.parameters["flush_output"] = True

    # Forms for matrices K and M
    # K_form = get_K_form_numint(V, d, dec_type_i)
    K_form_vector = fm.get_K_form_vector(V, X, d, dec_type_i, 0, H, n_ni, E, nu, xi_shear, k_shear)
    # psi_K_form = get_psi_K_form(X, d)
    # K_form_ufl_der = fe.derivative(psi_K_form, X, fe.TestFunction(V))
    M_form_1 = fm.get_M_form(V, True, rho, H)

    w_test, u_test, phi_test = fe.TestFunctions(V)
    F_form = p_0*w_test*fe.dx

    # # Forms for matrices K and M
    # K_form = 0.0
    # D1 = (E*H**3)/(12.0*(1.0 - nu**2))
    # D3 = (E*H)/(1.0 - nu**2)
    # kappa_tr, kappa_test = eps(S*theta_tr), eps(S*theta_test)
    # du_tr, du_test = eps(u_tr), eps(u_test)
    # K_form += fe.inner(D1*(nu*fe.div(S*theta_tr)*fe.Identity(2) + (1.0 - nu)*kappa_tr), kappa_test)*fe.dx
    # K_form += fe.inner(D3*(nu*fe.div(u_tr)*fe.Identity(2) + (1.0 - nu)*du_tr), du_test)*fe.dx
    # D2 = (E*k*H)/(2.0*(1.0 + nu))
    # #D2 = (mu*H*5.0)/6.0
    # K_form += D2*fe.inner(fe.grad(w_tr) + S*theta_tr, fe.grad(w_test) + S*theta_test)*dx_shear
    #
    # M_form_core = rho*H**3/12.0*fe.dot(S*theta_tr, S*theta_test)
    # M_form_core += rho*H*fe.dot(w_tr, w_test)
    # M_form_core += rho*H*fe.dot(u_tr, u_test)
    # M_form = M_form_core*fe.dx
    # M_form_quad = M_form_core*fe.dx(scheme="vertex", metadata={"degree": 1, "representation": "quadrature"})
    #
    # l_aux = fe.Constant(1.0)*u_test[0]*fe.dx

    # Assembling of consistent matrices
    # K_matrix = fe.assemble(K_form)
    K_vector = fe.assemble(K_form_vector)
    # K_matrix = fe.assemble(K_form_ufl_der)
    # [bci.apply(K_matrix) for bci in bc]
    [bci.apply(K_vector) for bci in bc]
    M_matrix = fe.assemble(M_form_1)
    [bci.apply(M_matrix) for bci in bc]
    F_vector = fe.assemble(F_form)
    [bci.apply(F_vector) for bci in bc]

    M_fce = fe.Function(V)
    M_vect = M_fce.vector()
    M_matrix.get_diagonal(M_vect)

    #d_form = get_d_form(W, X, d, "pham", hist)
    #solver_d = prepare_solver_d(hist)

    # Main time loop
    saveindex = save_rate
    t = t_start
    begin_t = time.time()
    fu.create_log_file(folder, "", mesh, nx, ny, E, nu, rho, ft_glass, lc, Gc_glass, m_imp=0, E_imp=0, nu_imp=0, h0=0, deg=-1, n_ni=0, k_res=0, xi_shear=0, k_shear=0)
    #fu.create_log_file(folder, point_type)
    while t < t_end:
        print("Time instant: " + str(t))

        # K_matrix = fe.assemble(K_form)
        # [bci.apply(K_matrix) for bci in bc]

        # Hertz nonlinear force
        # F_hertz = 0.25*k_0*mc_bracket(X_old.sub(3)(pp) - X_old.sub(0)(pp))**1.5
        # F_hertz = np.asscalar(0.25 * k_0 * mc_bracket(X_old.vector()[index_real] - X_old.vector()[index_hit]) ** 1.5)
        # F_hertz = 0

        # Update of RHS
        w_vector = -dt ** 2 * K_vector - dt**2*F_vector - M_matrix * (
                    X_old2.vector() - 2.0 * X_old.vector())
        # w_vector = -dt**2*K_matrix*X_old.vector() - M_matrix*(X_old2.vector() - 2.0*X_old.vector()) -dt**2*F_hertz*a_loc_vec
        [bci.apply(w_vector) for bci in bc]

        iMw = fe.Function(V)
        iMw.vector().set_local(w_vector.get_local() / M_vect.get_local())
        X.assign(iMw)

        #solve_damage("pham2", solver_d)

        K_vector = fe.assemble(K_form_vector)
        [bci.apply(K_vector) for bci in bc]

        if saving:
            if saveindex >= save_rate:
                w, u, phi = X.split(deepcopy=True)
                file.write(w, t)
                file_u.write(u, t)

                # solve_damage("pham2", solver_d)

                file_d.write(d, t)

                # K_matrix = fe.assemble(K_form)
                # [bci.apply(K_matrix) for bci in bc]
                # K_vector = fe.assemble(K_form_vector)
                # [bci.apply(K_vector) for bci in bc]

                impactor.append(0.0)
                #impactor.append(X.vector()[index_real])
                tt.append(t)

                saveindex = 0
            else:
                saveindex += 1

        X_old2.assign(X_old)
        X_old.assign(X)

        t += dt

    end_t = time.time()
    file.close()

    np.savetxt(folder + "/impactor.txt", np.column_stack([tt, impactor]))

    return end_t - begin_t, impactor, tt


# Geometry paramaters
L, B, H = 1.5, 1.5, 0.02  # Width, Height, Thickness

# Number of elements
nx, ny = 10, 10

# Degree orders
deg = 1  # Order of elements
deg_shear = 0  # Order of shear elements
n_ni = 40  # Number of gauss points for thickness integration

# Mesh
mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5 * L, 0.5 * B), nx, ny, "right")
fe.plot(mesh)
plt.show()

# Material parameters
E, nu, rho = 72e9, 0.22, 2500.0  # Young's module, Poisson ratio, density
#mu, lmbda = 0.5 * E / (1 + nu), E * nu / (1 + nu) / (1 - 2 * nu)  # Lame's coefficients
#lmbda = 2 * mu * lmbda / (lmbda + 2 * mu)  # Plane stress correction
ft_glass = 80.0e6  # Strengths of glass
# Fracture parameters
h_min = mesh.hmin()
lc = h_min
Gc_glass = lc * 8.0 / 3.0 * ft_glass ** 2 / E
k_res = 0.0
xi_shear = False
k_shear = 0.05
p_0 = 1.0

# Snes solver parameters
snes_solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver": {"linear_solver": "lu",
                                          "relative_tolerance": 1.0e-6,
                                          "absolute_tolerance": 1.0e-6,
                                          "maximum_iterations": 50,
                                          "report": True,
                                          "error_on_nonconvergence": False,
                                          "line_search": "basic"}}

# Time parameters
t_start = 0.0
t_end = 1.0e-3
dt = 2.0e-7

time, i1, t1 = explicit_solver("Plate_test_22", True, 1, "dist", "sd", "vol")

plt.plot(t1, i1, label="1.0e6")
plt.xlabel("Time [s]")
plt.ylabel("Impactor displacement [m]")
plt.show()
