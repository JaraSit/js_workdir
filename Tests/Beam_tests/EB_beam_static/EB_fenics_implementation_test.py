import fenics as fe
import matplotlib.pyplot as plt


def middle_point(x, on_boundary):
    return fe.near(x[0], 0.5)


def left_point(x, on_boundary):
    return fe.near(x[0], 0.0)


def right_point(x, on_boundary):
    return fe.near(x[0], L)


def boundary_points(x, on_boundary):
    return on_boundary


E = 1.0
I = 1.0
L = 1.0
alpha = 1.0

mesh = fe.IntervalMesh(100, 0.0, L)

V = fe.FunctionSpace(mesh, "P", 3)

bc1 = fe.DirichletBC(V, 1.0, middle_point)
bc2 = fe.DirichletBC(V, 0.0, boundary_points)
bc = [bc1, bc2]

u_test = fe.TestFunction(V)
u_tr = fe.TrialFunction(V)
u = fe.Function(V)

h = fe.CellDiameter(mesh)
h_avg = 0.5*(h('+') + h('-'))
n = fe.FacetNormal(mesh)

a = E*I*(u_test.dx(0)).dx(0)*(u_tr.dx(0)).dx(0)*fe.dx
a -= fe.inner(fe.avg(fe.div(fe.grad(u_tr))), fe.jump(fe.grad(u_test), n))*fe.dS
a -= fe.inner(fe.jump(fe.grad(u_tr), n), fe.avg(fe.div(fe.grad(u_test))))*fe.dS
a += alpha/h_avg*fe.inner(fe.jump(fe.grad(u_tr),n), fe.jump(fe.grad(u_test),n))*fe.dS
l = fe.Constant(1.0)*u_tr*fe.dx

fe.solve(a == l, u, bc)

fe.plot(u)
plt.show()
