import fenics as fe
import fenics_shells as fes


def calc_plate_fenics_shells():

    mesh = fe.UnitSquareMesh(32, 32)
    element = fe.MixedElement([fe.VectorElement("Lagrange", fe.triangle, 2),
                               fe.FiniteElement("Lagrange", fe.triangle, 1),
                               fe.FiniteElement("N1curl", fe.triangle, 1),
                               fe.FiniteElement("N1curl", fe.triangle, 1)])
    Q = fes.ProjectedFunctionSpace(mesh, element, num_projected_subspaces=2)

    Q_F = Q.full_space

    q_ = fe.Function(Q_F)
    theta_, w_, R_gamma_, p_ = fe.split(q_)
    q = fe.TrialFunction(Q_F)
    q_t = fe.TestFunction(Q_F)

    k = fe.sym(fe.grad(theta_))
    D = (E*t**3)/(24.0*(1.0 - nu**2))
    psi_b = D*((1.0 - nu)*fe.tr(k*k) + nu*(fe.tr(k))**2)
    psi_s = ((E*kappa*t)/(4.0*(1.0 + nu)))*fe.inner(R_gamma_, R_gamma_)

    f = fe.Constant(1.0)
    W_ext = fe.inner(f*t**3, w_)*fe.dx
    gamma = fe.grad(w_) - theta_

    # We instead use the :py:mod:`fenics_shells` provided :py:func:`inner_e` function::

    L_R = fes.inner_e(gamma - R_gamma_, p_)
    L = psi_b*fe.dx + psi_s*fe.dx + L_R - W_ext

    F = fe.derivative(L, q_, q_t)
    J = fe.derivative(F, q_, q)

    A, b = fes.assemble(Q, J, -F)

    # and apply boundary conditions::

    def all_boundary(x, on_boundary):
        return on_boundary

    # Boundary conditions.
    bcs = [fe.DirichletBC(Q, fe.Constant((0.0, 0.0, 0.0)), all_boundary)]

    for bc in bcs:
        bc.apply(A, b)

    # and solve the linear system of equations before writing out the results to
    # files in ``output/``::

    q_p_ = fe.Function(Q)
    solver = fe.PETScLUSolver("mumps")
    solver.solve(A, q_p_.vector(), b)
    fes.reconstruct_full_space(q_, q_p_, J, -F)

    save_dir = "output/"
    theta, w, R_gamma, p = u_.split()
    fields = {"theta": theta, "w": w, "R_gamma": R_gamma, "p": p}
    for name, field in fields.items():
        field.rename(name, name)
        field_file = XDMFFile("%s/%s.xdmf" % (save_dir, name))
        field_file.write(field)

    # We check the result against an analytical solution calculated using a
    # series expansion::

    from fenics_shells.analytical.simply_supported import Displacement

    w_e = Displacement(degree=3)
    w_e.t = t.values()
    w_e.E = E.values()
    w_e.p = f.values() * t.values() ** 3
    w_e.nu = nu.values()

    print("Numerical out-of-plane displacement at centre: %.4e" % w((0.5, 0.5)))
    print("Analytical out-of-plane displacement at centre: %.4e" % w_e((0.5, 0.5)))

    # Unit testing
    # ============
    #
    # ::

    def test_close():
        import numpy as np
        assert (np.isclose(w((0.5, 0.5)), w_e((0.5, 0.5)), atol=1E-3, rtol=1E-3))


E = fe.Constant(72.0e9)
nu = fe.Constant(0.22)
kappa = fe.Constant(5.0/6.0)
t = fe.Constant(0.02)
