import numpy as np
import fenics as fe
import js_workdir.utils.functions as fu

# --------------------
# Weak forms
# --------------------


def get_plate_form(V, qd_shear, E, nu, H):
    dx_shear = fe.dx(metadata={"quadrature_degree": qd_shear})

    k = 5.0/6.0
    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    w_tr, u_tr, phi_tr = fe.TrialFunctions(V)
    w_test, u_test, phi_test = fe.TestFunctions(V)

    K_form = 0.0

    D1 = (E * H ** 3) / (12.0 * (1.0 - nu ** 2))
    D2 = (E * k * H) / (2.0 * (1.0 + nu))
    D3 = (E * H) / (1.0 - nu ** 2)
    kappa_tr, kappa_test = fu.eps(S * phi_tr), fu.eps(S * phi_test)
    du_tr, du_test = fu.eps(u_tr), fu.eps(u_test)
    K_form += fe.inner(D1*(nu * fe.div(S * phi_tr) * fe.Identity(2) + (1.0 - nu) * kappa_tr), kappa_test) * fe.dx
    K_form += fe.inner(D3 * (nu * fe.div(u_tr) * fe.Identity(2) + (1.0 - nu) * du_tr), du_test) * fe.dx
    K_form += D2 * fe.inner(fe.grad(w_tr) + S * phi_tr, fe.grad(w_test) + S * phi_test) * dx_shear

    return K_form


def get_K_form_vector(V, u, d, dec_type, qd_shear, H, n_ni, E, nu, xi_shear, k_shear):
    dx_shear = fe.dx(metadata={"quadrature_degree": qd_shear})
    k = 5.0 / 6.0
    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
    mu, lmbda = 0.5 * E / (1 + nu), E * nu / (1 + nu) / (1 - 2 * nu)  # Lame's coefficients
    lmbda = 2 * mu * lmbda / (lmbda + 2 * mu)  # Plane stress correction

    w_tr, u_tr, phi_tr = fe.split(u)
    # w_tr, u_tr, phi_tr, imp_tr = fe.TrialFunctions(V)
    w_test, u_test, phi_test = fe.TestFunctions(V)
    # w_tr, u_tr, phi_tr = map_to_layers(lg_trial, H)
    # w_test, u_test, phi_test = map_to_layers(lg_test, H)

    K_form = 0.0

    num_int = np.linspace(-0.5 * H, 0.5 * H, n_ni)
    dh = abs(num_int[1] - num_int[0])

    for i in range(len(num_int) - 1):
        gauss = 0.5 * (num_int[i] + num_int[i + 1])
        u_z = u_tr + S * phi_tr * gauss
        u_z_test = u_test + S * phi_test * gauss
        # K_form += (1-d)**2*fe.inner(sigma(u_z), eps(u_z_test))*dh*fe.dx
        if dec_type == "hyb_sd":
            K_form += (1 - d) ** 2 * fe.inner(fu.sigma(u_z, lmbda, mu), fu.eps(u_z_test)) * dh * fe.dx
        elif dec_type == "vol":
            print("hej")
            K_form += fe.inner(fu.sigma_vd(u_z, d, lmbda, mu, k_res=0), fu.eps(u_z_test)) * dh * fe.dx
        elif dec_type == "sd":
            K_form += fe.inner(fu.sigma_sd(u_z, d, lmbda, mu, k_res=0), fu.eps(u_z_test)) * dh * fe.dx
        else:
            raise Exception("Only hyb_sd/vol/sd decomposition implemented!")

    # K_form *= (1-d)**2
    # u_form += D2 * fe.inner(fe.grad(w_) + S * theta_, fe.grad(w_test) + S * theta_test) * dx_shear
    # u_form -= fe.Constant(0.0) * w_test * fe.dx

    # D1 = (E*H**3)/(12.0*(1.0 - nu**2))
    D2 = (E * k * H) / (2.0 * (1.0 + nu))
    # D3 = (E*H)/(1.0 - nu**2)
    # K_form += fe.inner(D1*(nu*fe.div(S*phi_tr)*fe.Identity(2) + (1.0 - nu)*kappa_tr),kappa_test)*fe.dx
    # K_form += fe.inner(D3*(nu*fe.div(u_tr)*fe.Identity(2) + (1.0 - nu)*du_tr), du_test)*fe.dx
    # K_form += ((1-d)**2 + k_res)*D2*fe.inner(fe.grad(w_tr) + S*phi_tr, fe.grad(w_test) + S*phi_test)*dx_shear
    # K_form += D2*fe.inner(fe.grad(w_tr) + S*phi_tr, fe.grad(w_test) + S*phi_test)*dx_shear

    if xi_shear:
        K_form += D2 * fe.inner(fe.grad(w_tr) + S * phi_tr, fe.grad(w_test) + S * phi_test) * dx_shear
    else:
        print("hej2")
        K_form += ((1 - d) ** 2 + k_shear) * D2 * fe.inner(fe.grad(w_tr) + S * phi_tr,
                                                           fe.grad(w_test) + S * phi_test) * dx_shear

    return K_form


def get_M_form(V, lump, rho, H):
    if lump:
        dx_m = fe.dx(scheme="vertex", metadata={"degree": 1, "representation": "quadrature"})
    else:
        dx_m = fe.dx

    w_tr, u_tr, phi_tr = fe.TrialFunctions(V)
    w_test, u_test, phi_test = fe.TestFunctions(V)
    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    M_form = 0.0

    M_form += rho * H ** 3 / 12.0 * fe.dot(S * phi_tr, S * phi_test) * dx_m
    M_form += rho * H * fe.dot(w_tr, w_test) * dx_m
    M_form += rho * H * fe.dot(u_tr, u_test) * dx_m

    return M_form


def get_1D_form_staggered_u(type_1, d, V, E, A):
    if type_1 == "b":
        u_test = fe.TestFunction(V)
        u_tr = fe.TrialFunction(V)
        E_du = (1 - d)**2*E*A*fe.inner(fe.grad(u_test), fe.grad(u_tr))*fe.dx
        return E_du
    else:
        raise Exception("Only bourdin(b) type implemented!")


def get_1D_form_staggered_d(type_1, u, V, E, A, lc, Gc):
    if type_1 == "b":
        d_test = fe.TestFunction(V)
        d_tr = fe.TrialFunction(V)
        E_ds = -0.5*E*A*fe.inner(fe.grad(u), fe.grad(u))*fe.inner(1.0 - d_tr, d_test)*fe.dx + Gc*(
                    1.0/lc*fe.inner(d_tr, d_test) +
                    lc*fe.inner(fe.grad(d_tr), fe.grad(d_test)))*fe.dx
        return E_ds
    else:
        raise Exception("Only bourdin(b) type implemented!")
