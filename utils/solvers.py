import numpy as np
import fenics as fe
import functions as fu

# --------------------
# Solvers
# --------------------


def prepare_solver_d(hist_i, d, W, d_form, snes_params):
    lower = d
    upper = fe.interpolate(fe.Constant(1.0), W)

    # Solution of damage formulation
    H = fe.derivative(d_form, d, fe.TrialFunction(W))

    problem = fe.NonlinearVariationalProblem(d_form, d, [], H)
    problem.set_bounds(lower, upper)

    solver = fe.NonlinearVariationalSolver(problem)
    solver.parameters.update(snes_params)
    return solver


def solve_damage(d_type, solver, d):
    if d_type == "pham":
        solver.solve()
    elif d_type == "bourdin":
        raise Exception("Only pham type implemented!")
        #fe.solve(fe.lhs(d_form) == fe.rhs(d_form), d, [])
        #hist.assign(local_project(max_fce(hist, get_positive_energy(X)), W0))